# a deque can be used to check a palindrom by removing chars from either side and determining if they're the same

from Deque import Deque


def palChecker(astring):
    chardeque = Deque()

    for ch in astring:
        chardeque.addRear(ch)

    equal = True

    while chardeque.size() > 1 and equal:
        first = chardeque.removeFront()
        last = chardeque.removeRear()
        if first != last:
            equal = False

    return equal


print(palChecker("lsdkjfskf"))
print(palChecker("radar"))
