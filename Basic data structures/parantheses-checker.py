# check if the given input of parantheses are balanced using the stack implementation

from Stack import Stack

def parChecker(SymbolString):
    s = Stack()
    index = 0
    balanced = True

    while index < len(SymbolString) and balanced:
        symbol = SymbolString[index]

        if symbol == '(':
            s.push(symbol)
        else:
            if s.isEmpty():
                balanced = False
            else:
                s.pop()

        index += 1

    if balanced and s.isEmpty():
        return True
    else:
        return False


print(parChecker('((()))'))
print(parChecker('(())))'))
