# infix = A + B
# postfix = A B +
# Prefix = + A B

# A * B + C * D should give A B * C D * +

from Stack import Stack

def postFixConvertor(infix):
    postFixStack = Stack()
    precedence = {}
    precedence['('] = 1
    precedence['/'] = 3
    precedence['*'] = 3
    precedence['+'] = 2
    precedence['-'] = 2
    postFixList = []
    tokenList = infix.split()

    for token in tokenList:
        if token in "ABCDEFGHIJKLMNOPQRSTUVWXYZ" or token in "0123456789":
            postFixList.append(token)
        elif token == '(':
            postFixStack.push(token)
        elif token == ')':
            topToken = postFixStack.pop()

            while topToken != '(':
                postFixList.append(topToken)
                topToken = postFixStack.pop()

        else:
            while (not postFixStack.isEmpty()) and (precedence[postFixStack.peep()] >= precedence[token]):
                postFixList.append(postFixStack.pop())
            postFixStack.push(token)

    while not postFixStack.isEmpty():
        postFixList.append(postFixStack.pop())

    return " ".join(postFixList)

print(postFixConvertor("A * B + C * D"))
print(postFixConvertor("( A + B ) * C - ( D - E ) * ( F + G )"))
print(postFixConvertor("( A + B ) * ( C + D )"))
print(postFixConvertor("10 + 3 * 5 / (16 - 4)"))
