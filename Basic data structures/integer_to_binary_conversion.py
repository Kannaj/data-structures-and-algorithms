# stacks can also be used for decimal to binary conversion

from Stack import Stack

def divideBy2(decimal):

    revBin = Stack()

    while decimal>0:
        revBin.push(decimal % 2)
        decimal = decimal // 2

    binary = ""

    while not revBin.isEmpty():
        binary = binary + str(revBin.pop())

    return binary

print(divideBy2(42))
