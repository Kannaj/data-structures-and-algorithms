# simulate the hot potato game - 'pass the parcel' game for us
# my version using recursion

from Queue import Queue

players = Queue()

playerList = ['KJ','House','Steven Wilson','Thom Yorke','Geddy Lee','Mikael Akerfeldt','Maynard James Keenan']

for name in playerList:
    players.enqueue(name)


def playGame(players,seconds):
    while seconds != 0 and players.size() > 1:
        players.enqueue(players.dequeue())
        seconds -= 1

    if players.size() != 1:
        name = players.dequeue()
        print(name + ' has been eliminated')
        print('remaining players: ',players.view())

        return playGame(players,seconds)
    else:
        print('final winner is: ',players.dequeue())

playGame(players,10)

# tutorial version
def hotPotato(namelist, num):
    simqueue = Queue()
    for name in namelist:
        simqueue.enqueue(name)

    while simqueue.size() > 1:
        for i in range(num):
            simqueue.enqueue(simqueue.dequeue())

        simqueue.dequeue()

    return simqueue.dequeue()
    
print(hotPotato(["Bill","David","Susan","Jane","Kent","Brad"],7))
