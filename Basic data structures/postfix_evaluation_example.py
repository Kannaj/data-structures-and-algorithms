# postfix notation of '7 8 + 3 2 + /'
# evaluate a postfix notation
from Stack import Stack

def postFixEvaluation(postfix):
    postFixList = postfix.split()
    operandStack = Stack()

    for token in postFixList:
        if token in '0123456789':
            operandStack.push(int(token))
            print('operandStack is now ,',operandStack.view())

        else:
            operand2 = operandStack.pop()
            operand1 = operandStack.pop()

            result = doMath(token,operand1,operand2)
            operandStack.push(result)
            print('result is now : ',result,' : ',operand2,' : ',operand1)

    return operandStack.pop()

def doMath(token,op1,op2):
    print(locals())
    if token == '+':
        return op1 + op2
    elif token == '-':
        return op1 - op2
    elif token == '*':
        return op1 * op2
    else:
        return op1 / op2

print(postFixEvaluation('7 8 + 3 2 + /'))
