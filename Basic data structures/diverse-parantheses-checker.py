# similar to parantheses-checker but also includes '{}' and '[]'
from Stack import Stack

def parDivChecker(symbolString):
    s = Stack()
    balanced = True
    index = 0

    while index < len(symbolString) and balanced:
        symbol = symbolString[index]
        if symbol in '({[':
            s.push(symbol)
        else:
            if s.isEmpty():
                balanced = False
            else:
                last = s.pop()
                if not matches(last,symbol):
                    balanced = False
        index += 1

    if balanced and s.isEmpty():
        return True
    else:
        return False


# check if opening and closing symbol match {} or () or []
def matches(last,symbol):
    opens = '([{'
    closes = ')]}'
    return opens.index(last) == closes.index(symbol)


print(parDivChecker('{{([][])}()}'))
print(parDivChecker('[{()]'))
print(parDivChecker('({[]})'))
